package yardsuntea.thanaphat.lab2;

public class AreaVolumeConeSolver {

	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length == 3) {

			double r = Double.parseDouble(args[0]);
			double s = Double.parseDouble(args[1]);
			double h = Double.parseDouble(args[2]);
			double Surface = (java.lang.Math.PI * r * s) + (java.lang.Math.PI * r * r);
			// calculate surface
			double Volume = (1.0 / 3) * java.lang.Math.PI * r * r * h;
			// calculate volume

			System.out.println("For cone with r " + r + " s " + s + " h " + h);
			System.out.println("Surface area is " + Surface + " volume is " + Volume);

		} else {
			System.err.println("AreaVolumeconeSolver <r> <s> <h>"); // error message
		}

	}

}
