package yardsuntea.thanaphat.lab2;

public class UsingStringAPI {

	/** Write by Thanaphat Yardsuntea 
	 *  StudentID 553040261-9
	 *  Sec 2
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String schoolName = args[0];
		String s = "school"; 
		String c = "college";
		String low = schoolName.toLowerCase(); // change to lower
		int a = low.indexOf(s); // for search index of String s
		int b = low.indexOf(c); // for search index of String c

		if(a >- 1){
		System.out.println(schoolName + " has " + s + " " + a);
		}
		else if(b > -1){
			System.out.println(schoolName + " has " + c + " " + b);
		}
		else {
			System.out.println(schoolName + " dose not has contain " + s + " or " + c);
		}

	}

}
