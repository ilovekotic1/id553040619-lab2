package yardsuntea.thanaphat.lab2;

public class DataTypes {

	/** Write by Thanaphat Yardsuntea 
	 *  StudentID 553040261-9
	 *  Sec 2
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String a1 = args[0];
		String a2 = args[1];
		boolean a3 = true;

		String c1 = a2.substring(0, 2);
		String c2 = a2.substring(a2.length() - 2, a2.length()); 

		int x = Integer.parseInt(c2);
		String oct = Integer.toOctalString(x); // Octal value
		String hex = Integer.toHexString(x); // Hexadecimal value

		long l = Long.parseLong(c2); // Change String to Long value

		float f1 = Float.parseFloat(c1); // Change String to Float value
		float f2 = Float.parseFloat(c2);
		float f3 = f2 / 100;
		float f4 = f1 + f3;

		double d1 = Double.parseDouble(c1); // Change String to Double value
		double d2 = Double.parseDouble(c2);
		double d3 = d2 / 100;
		double d4 = d1 + d3;

		System.out.println("My name is " + a1);
		System.out.println("My student ID was " + a2);
		System.out.println(a1.charAt(0) + " " + a3 + " " + oct + " " + hex);
		System.out.println(l + " " + f4 + " " + d4);

	}

}
