package yardsuntea.thanaphat.lab2;

import java.util.Arrays; // import function java.util.Arrays

public class SortNumbers {

	/** Write by Thanaphat Yardsuntea 
	 *  StudentID 553040261-9
	 *  Sec 2
	 *  
	 * @param args
	 */
	public static void main(String[] args) {
		String[] a = { args[0], args[1], args[2], args[3] };
		Arrays.sort(a); // for sort low numbers to high numbers

		System.out.println("For the input numbers: ");
		System.out.print(args[0]);
		System.out.print(" " + args[1]);
		System.out.print(" " + args[2]);
		System.out.println(" " + args[3]);
		System.out.println("Sorted number: ");
		System.out.println(Arrays.toString(a));
	}

}
