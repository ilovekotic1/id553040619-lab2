package yardsuntea.thanaphat.lab2;

public class Greetings {

	/** Write by Thanaphat Yardsuntea 
	 *  StudentID 553040261-9
	 *  Sec 2
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length == 3) {
			System.out.println("My favorite teacher's name is " + args[0]);
			System.out.print("The teacher taught me at " + args[1]);
			System.out.println(" which is in " + args[2]);
		} else {
			System.err.println("Greetings <teacher name> <school name> <province name>");
		}

	}

}
