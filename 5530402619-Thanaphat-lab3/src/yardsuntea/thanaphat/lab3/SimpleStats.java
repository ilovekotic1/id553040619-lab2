package yardsuntea.thanaphat.lab3;

import java.util.Arrays;

public class SimpleStats {

	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2 
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		double sum = 0;
		int numGPAs = Integer.parseInt(args[0]);
		double nums[] = new double[numGPAs];

		System.out.println("For in the input: ");
		for (int i = 0; i < numGPAs; i++) {
			nums[i] = Double.parseDouble(args[i + 1]);
			System.out.print(args[i + 1] + " ");
			sum = sum + nums[i];
		}

		Arrays.sort(nums);
		System.out.println();
		System.out.println("stats:");
		System.out.println("Avg GPAs is " + sum / numGPAs);
		System.out.println("Min GPAs is " + nums[0]);
		System.out.println("Max GPAs is " + nums[numGPAs - 1]);

	}

}
