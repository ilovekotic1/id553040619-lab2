package yardsuntea.thanaphat.lab3;

public class DivisorChecker {

	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 * 
	 * @param args
	 */
	
	static int d1;
	static int d2;
	static int min;
	static int max;
	
	public static void main(String[] args) {
		if(args.length != 4){
			System.err.println("DivisorChecker <divisor 1> <divisor 2> " + " <min number> <max number> ");
			System.exit(0);
		}
		d1 = Integer.parseInt(args[0]);
		d2 = Integer.parseInt(args[1]);
		min = Integer.parseInt(args[2]);
		max = Integer.parseInt(args[3]);
		displayNumber();
		}

	private static void displayNumber() {
		for (int x = min; x <= max; x++) {
			if (x % d2 == 0 && x % d1 == 0) {
				System.out.print("");
			} else if (x % d2 == 0) {
				System.out.println(x);
			}
		}
	}
		
	}

